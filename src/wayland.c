/*
 * Copyright © 2022 Leandro Ribeiro <leandrohr@riseup.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#include <wayland-client.h>
#include <wayland-egl.h>
#include <EGL/egl.h>

#include "wayland-protocols/xdg-shell-client-protocol.h"
#include "wayland-protocols/linux-dmabuf-unstable-v1-client-protocol.h"

#include "utils.h"

#define NUM_BUFFERS 4

bool supports_argb8888 = false;

struct display
{
        struct wl_display *wl_display;
        struct wl_registry *wl_registry;
        struct wl_compositor *wl_compositor;
        struct wl_shm *wl_shm;
        struct xdg_wm_base *xdg_wm_base;
        struct zwp_linux_dmabuf_v1 *zwp_linux_dmabuf_v1;
};

struct buffer
{
        struct wl_buffer *wl_buffer;
        uint32_t width, height;
};

struct window
{
        struct wl_surface *wl_surface;
        uint32_t width, height;

        struct buffer buffers[NUM_BUFFERS];
};

static void
shm_format(void *data, struct wl_shm *wl_shm, uint32_t format)
{
        if (format == WL_SHM_FORMAT_ARGB8888)
                supports_argb8888 = true;
}

struct wl_shm_listener shm_listener =
{
        .format = shm_format,
};

static void
global_remove_listener(void *data, struct wl_registry *wl_registry, uint32_t name)
{
        /* do nothing */
}

static void
global_listener(void *data, struct wl_registry *wl_registry,
                uint32_t name, const char *interface, uint32_t version)
{
        struct display *display = data;

        if (strcmp(interface, "wl_compositor") == 0)
        {
                display->wl_compositor =
                        wl_registry_bind(display->wl_registry, name,
                                         &wl_compositor_interface, MIN(version, 4));
        }
        else if (strcmp(interface, "wl_shm") == 0)
        {
                display->wl_shm =
                        wl_registry_bind(display->wl_registry, name,
                                         &wl_shm_interface, 1);
                wl_shm_add_listener(display->wl_shm, &shm_listener, NULL);
        }
        else if (strcmp(interface, "xdg_wm_base") == 0)
        {
                display->xdg_wm_base =
                        wl_registry_bind(display->wl_registry, name,
                                         &xdg_wm_base_interface, 1);
        }
        else if (strcmp(interface, "zwp_linux_dmabuf_v1") == 0)
        {
                display->zwp_linux_dmabuf_v1 =
                        wl_registry_bind(display->wl_registry, name,
                                         &zwp_linux_dmabuf_v1_interface, 3);
        }
}

struct wl_registry_listener registry_listener =
{
        .global = global_listener,
        .global_remove = global_remove_listener,
};

static struct display *
display_create(void)
{
        struct display *display;
        int ret;
        
        display = calloc(1, sizeof(*display));
        if (!display)
                return NULL;

        display->wl_display = wl_display_connect(NULL);
        if (!display->wl_display)
                goto err;

        display->wl_registry = wl_display_get_registry(display->wl_display);
        if (!display->wl_registry)
                goto err_reg;

        ret = wl_registry_add_listener(display->wl_registry, &registry_listener, display);
        if (ret < 0)
                goto err_list;

        return display;

err_list:
        wl_registry_destroy(display->wl_registry);
err_reg:
        wl_display_disconnect(display->wl_display);
err:
        free(display);
        return NULL;
}

static void
display_destroy(struct display *display)
{
        wl_display_disconnect(display->wl_display);
        free(display);
}

static void
buffer_init(struct buffer *buffer, int width, int height)
{
        buffer->width = width;
        buffer->height = height;
}

static struct window *
window_create(struct display *display, int width, int height)
{
        struct window *window;
        unsigned int i;

        window = calloc(1, sizeof(*window));
        if (!window)
                return NULL;

        window->width = width;
        window->height = height;

        for (i = 0; i < NUM_BUFFERS; i++)
        {
                buffer_init(&window->buffers[i], width, height);
        }

        window->wl_surface = wl_compositor_create_surface(display->wl_compositor);
        if (!window->wl_surface)
                goto err;

        wl_display_roundtrip(display->wl_display);

        return window;

err:
        free(window);
        return NULL;
}

int
main(int argc, char **argv)
{
        struct display *display;
        struct window *window;

        display = display_create();
        if (!display)
                return 0;

        /* get globals */
        wl_display_roundtrip(display->wl_display);
        /* get events from globals */
        wl_display_roundtrip(display->wl_display);

        assert(supports_argb8888);

        window = window_create(display, 800, 600);
        if (!window)
                goto err;

        display_destroy(display);

        return 0;

err:
        display_destroy(display);
        return 0;
}